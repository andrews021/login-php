CREATE DATABASE login;
USE login;

CREATE TABLE "login"(
  "usuario_id" INT NOT NULL,
  "usuario" VARCHAR(200) NOT NULL,
  "senha" VARCHAR(32) NOT NULL,
  "nome" VARCHAR(100) NOT NULL,
  "data_cadastro" DATE NOT NULL,
  "email" varchar (50) not null
  PRIMARY KEY ("usuario_id"));

INSERT INTO "login" ("usuario_id","usuario","senha", "nome", "data_cadastro", "email") VALUES (1,'andrews','1234', 'ANDREWS', '2020-01-11', 'andrewsvinicius02@gmail.com');